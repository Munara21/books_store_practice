import React from 'react';

import ShoppingCartTable from '../shopping-cart-table/shopping-cart-table';
import BookList from "../book-list/book-list";
//import {connect} from "react-redux";
//import store from "../../store";
//import mapDispatchToProps from "react-redux/lib/connect/mapDispatchToProps";

const HomePage = (props) => {

    console.log('Props: ', props)

    return (
        <div>
            <BookList/>
            <ShoppingCartTable />
        </div>
    );
};


export default HomePage;



/*
const mapStateToProps = (state) => {

    console.log('state: ', state)

    return {
        error: state.error
    }
}

const connect2 = (mapStateToProps, mapDispatchToProps) => {

    const state = store.getState()
    const dispatch = store.dispatch

    let props1 = {}
    let props2 = {}

    if (mapStateToProps){
        props1 = mapStateToProps(state)
    }

    if (mapDispatchToProps){
        props2 = mapDispatchToProps(dispatch)
    }

    let props = {...props1, ...props2}

    return (Component) => {
        return <>
            <Component {...props}/>
        </>
    }

}*/










































