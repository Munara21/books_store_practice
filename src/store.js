import {createStore} from "redux";

import reducer from "./reducers";

const store = createStore(
    reducer,
    {
        books:[],
        loading: false,
        error: null,
        cartItems: [],
        orderTotal: 0
    },
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;