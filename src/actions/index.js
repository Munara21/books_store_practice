const booksLoaded = (newBooks) => {
    return {
        type: 'FETCH_BOOKS_SUCCESS',
        payload: newBooks
    };
};

const booksRequested =()=>{
    return {
        type: 'FETCH_BOOKS_REQUEST'
    }
};

const booksFail =(error)=>{
    return{
        type: 'FETCH_BOOKS_FAILURE',
        payload: error
    }
};
export const bookAddedToCart = (bookId) => {
    return {
        type: 'BOOK_ADDED_TO_CART',
        payload: bookId
    };
};

export const bookRemoveFromCart = (bookId) => {
    return {
        type: 'BOOK_REMOVE_FROM_CART',
        payload: bookId
    };
};

export const allBooksRemoveFromCart = (bookId) => {
    return {
        type: 'ALL_BOOKS_REMOVE_FROM_CART',
        payload: bookId
    };
};

const fetchBooks = (bookstoreService, dispatch) => () =>{
    dispatch (booksRequested());
    bookstoreService.getBooks()
        .then((data) => {
            console.log('data: ', data)
            dispatch(booksLoaded(data))
        })
        .catch((err) => dispatch (booksFail(err)));
}


export {
    fetchBooks
};